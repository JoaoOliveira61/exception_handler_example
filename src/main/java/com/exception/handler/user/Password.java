package com.exception.handler.user;

import java.util.Arrays;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

public class Password {

	@NotBlank
	private String currentPassword;
	private boolean isPasswordValid;
	@Valid
	private SecurityQuestion[] securityQuestions;
	
	public Password() {
		
	}
	
	public Password(String currentPassword, boolean isPasswordValid, SecurityQuestion[] securityQuestions) {
		super();
		this.currentPassword = currentPassword;
		this.isPasswordValid = isPasswordValid;
		this.securityQuestions = securityQuestions;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public boolean isPasswordValid() {
		return isPasswordValid;
	}

	public void setPasswordValid(boolean isPasswordValid) {
		this.isPasswordValid = isPasswordValid;
	}

	public SecurityQuestion[] getSecurityQuestions() {
		return securityQuestions;
	}

	public void setSecurityQuestions(SecurityQuestion[] securityQuestions) {
		this.securityQuestions = securityQuestions;
	}

	@Override
	public String toString() {
		return "Password [currentPassword=" + currentPassword + ", isPasswordValid=" + isPasswordValid
				+ ", securityQuestions=" + Arrays.toString(securityQuestions) + "]";
	}
}
