package com.exception.handler.user;

import org.hibernate.validator.constraints.NotBlank;

public class SecurityQuestion {

	@NotBlank
	private String question;
	@NotBlank
	private String answer;
	
	public SecurityQuestion() {
		
	}
	
	public SecurityQuestion(String question, String answer) {
		super();
		this.question = question;
		this.answer = answer;
	}
	
	public String getQuestion() {
		return question;
	}
	
	public void setQuestion(String question) {
		this.question = question;
	}
	
	public String getAnswer() {
		return answer;
	}
	
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Override
	public String toString() {
		return "SecurityQuestion [question=" + question + ", answer=" + answer + "]";
	}
	
	
}