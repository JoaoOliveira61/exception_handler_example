package com.exception.handler.user;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class User {

	@NotBlank
	private String name;
	@NotNull
	private Integer age;
	@NotBlank
	private String username;
	@Valid
	private Password password;
	
	public User() {
		
	}
	
	public User(String name, Integer age, String username, Password password) {
		super();
		this.name = name;
		this.age = age;
		this.username = username;
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Password getPassword() {
		return password;
	}

	public void setPassword(Password password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", age=" + age + ", username=" + username + ", password=" + password + "]";
	}
}
