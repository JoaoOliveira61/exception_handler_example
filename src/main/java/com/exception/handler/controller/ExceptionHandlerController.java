package com.exception.handler.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.exception.handler.user.User;

@Controller
public class ExceptionHandlerController {

	@RequestMapping(value = "/getUser", method = RequestMethod.GET)
	@ResponseBody
	public User getUser(String username) {
		return null;
	}

	@RequestMapping(value = "/postUser", method = RequestMethod.POST)
	@ResponseBody
	public boolean postUser(Model model, @Valid @RequestBody User user, BindingResult result) {
		if (result.hasErrors()){
			List<ObjectError> errors = result.getAllErrors();
			
			for (ObjectError error : errors) {
				System.out.println("Code: " + error.getCode() + " , ObjectName; " + error.getObjectName() 
					+ " , Message: " + error.getDefaultMessage());
			}
		}
		else {
			System.out.println(user.toString());
		}
		
		return true;
	}

}
