package com.exception.handler;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.exception.handler.user.Password;
import com.exception.handler.user.SecurityQuestion;
import com.exception.handler.user.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExceptionHandlerExampleApplicationTests {

	private User user1;
	
	@Before
	public void before() throws JsonProcessingException {
		SecurityQuestion[] secQuests = new SecurityQuestion[2];
		secQuests[0] = new SecurityQuestion("Question 1", "Answer 1");
		secQuests[1] = new SecurityQuestion("Question 2", "Answer 2");
		
		Password pass = new Password("password", true, secQuests);
		
		this.user1 = new User("John Doe", 20, "johndoe31", pass);
		
		ObjectMapper mapper = new ObjectMapper();
		System.out.println(mapper.writeValueAsString(user1));
	}
	
	@Test
	public void contextLoads() {
	}

}
